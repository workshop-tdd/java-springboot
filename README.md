Advisor App
===========

This project is based on Java 8 and Maven 3.

* Build: mvn clean compile
* Test: mvn clean test
* Run the app: mvn clean spring-boot:run
* Package: mvn clean package

The following command can be used to run in a docker environment.

    docker run -it --rm -v$PWD:/workspace maven:3-jdk-8-alpine /bin/bash